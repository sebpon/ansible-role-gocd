# Ansible role to deploy GOCD

Info about GOCD : https://www.gocd.org/

## Server and Agents

GoCD has 2 types of installation

* Server: this is the main installation type and provides GoCD server to which users connect to define / organize / start integration or deployment pipelines.  
* Agent: GoCD agents are where the *real* work occurs. Agents are machines were the tasks / jobs defined in the pipeline stages are effectively running.

A minimal installation of GoCD will thus consists in one GoCD server and one GoCD agent.  

Installation type is defined through the ansible variable `gocd_node_role`. By default the role installs the GoCD server.  
If you want to install an agent, you need to specify `gocd_node_role = agent` in your ansible playbook.

## Dependencies

GoCD needs a Java 1.8 deployment to work correctly (server and agent). Use the ansible-role-java role to comply with this requirement.   
Client software for software version control system (git, svn, etc.) must also be installed on all GoCD nodes (server and agent).

## Default locations

### GoCD server

`/var/lib/go-server`       contains the binaries and database  
`/etc/go`                  contains the pipeline configuration files  
`/var/log/go-server`       contains the server logs  
`/usr/share/go-server`     contains the start script  
`/etc/default/go-server`   contains all the environment variables with default values.

### GoCD agent

`/var/lib/go-agent`        contains the binaries
`/usr/share/go-agent`      contains the start script
`/var/log/go-agent`        contains the agent logs
`/etc/default/go-agent`    contains all the environment variables with default values.

These variable values can be changed as per requirement.  